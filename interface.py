from tkinter import *
from tkinter.messagebox import *
import tkinter.font as tkFont

from app import *

def test_int(valeur):
    if valeur.isdigit():
        return int(valeur)
    else:
        return 0

root = Tk()
helv14 = tkFont.Font(family='Helvetica',size=14)
root.title("Le chiffre de Cesar")
icon = PhotoImage(file='icon.png')
root.iconphoto(False, icon) 



def button_cryptage_callback():
    retour.set(cryptage_non_circulaire(value.get(),test_int(nbr_decalage.get())))
        

def button_decryptage_callback():
    retour.set(decryptage_non_circulaire(value.get(),test_int(nbr_decalage.get())))
 
def button_cryptage_circulaire_callback():
     retour.set(circulaire(value.get(),test_int(nbr_decalage.get()),'cryptage'))
    
def button_decryptage_circulaire_callback():
    retour.set(circulaire(value.get(),test_int(nbr_decalage.get()),'decryptage'))

value = StringVar()
value.set("Entrez votre phrase...")


input_r = Entry(root, textvariable=value)
input_r.grid(column=1, row=0,pady=15,padx=15)
input_r.config( width = 30, font = helv14, ) 

retour = StringVar()

output_resultat = Entry(root, textvariable = retour)
output_resultat.grid(column = 3, row=0,pady=15,padx=15) 
output_resultat.config(width = 30, font = helv14)

nbr_decalage = StringVar()
nbr_decalage.set("Decalage ")

input_int = Entry(root,textvariable= nbr_decalage)
input_int.grid(column = 2 , row = 1 , padx = 15 , pady = 15)

button_cryptage = Button(root, text="Cryptage", command=button_cryptage_callback)
button_cryptage.grid(row=1, column=0, padx=15, pady=15)
button_cryptage.config( height = 2, bg = 'orange', fg = 'black', font = helv14)


button_cryptage_circulaire = Button(root, text="Cryptage Circulaire", command=button_cryptage_circulaire_callback)
button_cryptage_circulaire.grid(row=1, column=1, padx=15, pady=15)
button_cryptage_circulaire.config( height = 2, bg = 'dark orange', fg = 'black', font = helv14)


button_decryptage = Button(root, text="Decryptage",command=button_decryptage_callback)
button_decryptage.grid(row = 1 ,column = 3, padx=15, pady=15)
button_decryptage.config( height = 2, bg = 'darkgreen', fg = 'black', font = helv14)

button_decryptage_circulaire = Button(root, text="Decryptage circulaire",command=button_decryptage_circulaire_callback)
button_decryptage_circulaire.grid(row = 1 ,column = 4, padx=15, pady=15)
button_decryptage_circulaire.config( height = 2, bg = 'green', fg = 'black', font = helv14)

root.mainloop()