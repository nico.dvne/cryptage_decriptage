import string

list_minuscule = list(string.ascii_lowercase)
list_majuscule = list(string.ascii_uppercase)

def cryptage_non_circulaire(chaine,decalage):
    
    result = [] 

    for lettre in chaine:
        result.append(chr(ord(lettre)+decalage)) #chaine criptée
    resultat = "".join(result) # la liste devient une chaine de caractères
    return resultat


def decryptage_non_circulaire(chaine,decalage):
    
    result = [] 
    
    for lettre in chaine:    
        result.append(chr(ord(lettre)-decalage))
    resultat = "".join(result) # la liste devient une chaine de caractères
    return resultat


def circulaire(chaine,decalage,etat):
    new_liste_minuscule = list_minuscule[decalage:]+list_minuscule[:decalage] #ascii de 65 a 90
    new_liste_majuscule = list_majuscule[decalage:]+list_majuscule[:decalage] #ascii de 97 a 122
    result = []

    for lettre in chaine:
        if lettre in new_liste_minuscule:  #si la lettre est une minuscule  
            result.append(transcription(lettre,decalage,new_liste_minuscule,'cryptage'))
        elif lettre in new_liste_majuscule: #si la lettres est une majuscule
            result.append(transcription(lettre,decalage,new_liste_majuscule,'cryptage'))
        else:#gestion cara special
            result.append(chr(ord(lettre)+decalage))                 

    resultat = "".join(result)

    return resultat


def transcription(lettre,decalage,liste,etat):
    to_return = ''
    if(ord(lettre)==32): #testons si le charactère est un espace
        to_return = " "
    elif lettre.isdigit(): #testons si le caractère est un chiffre
        to_return = lettre
    else :
        if etat == 'cryptage':
            to_return = liste[(liste.index(lettre)+decalage)%len(liste)]
        else:
            to_return = liste[(liste.index(lettre)-decalage)%len(liste)]
    
    return to_return
    