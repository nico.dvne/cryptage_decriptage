import unittest
from app import *
import string

list_minuscule = list(string.ascii_lowercase)
list_majuscule = list(string.ascii_uppercase)

class TestCryptageFunctions(unittest.TestCase):
   
    
    def test_cryptage(self):
        self.assertEqual(cryptage_non_circulaire('abc',8), 'ijk')
        self.assertEqual(cryptage_non_circulaire('bcd',8), 'jkl')
        self.assertNotEqual(cryptage_non_circulaire('abc',8), 'samy')

    def test_decryptage(self):
        self.assertEqual(decryptage_non_circulaire('ijk',8), 'abc')
        self.assertEqual(decryptage_non_circulaire('jkl',8), 'bcd')
        self.assertNotEqual(cryptage_non_circulaire('abc',8), 'samy')

    def test_cryptage_circulaire(self):
        self.assertEqual(circulaire("Nicolas",1,'cryptage'),"Ojdpmbt")
        self.assertEqual(circulaire("Nicolas",0,'cryptage'),'Nicolas')
        self.assertEqual(circulaire("Julien",10,'cryptage'),'Tevsox')
        self.assertEqual(circulaire('abcdefghijklmnopqrstuvwxyz',26,'cryptage'),'abcdefghijklmnopqrstuvwxyz')

    def test_decryptage_circulaire(self):
        self.assertEqual(circulaire("Ojdpmbt",1,'decryptage'),'Nicolas')
        self.assertEqual(circulaire("def",3,'decryptage'),'abc')
        self.assertEqual(circulaire('samy',10,'decryptage'),'iqco')

    def test_transcription(self):
        decalage = 3

        new_liste_minuscule = list_minuscule[decalage:]+list_minuscule[:decalage] #ascii de 65 a 90
        new_liste_majuscule = list_majuscule[decalage:]+list_majuscule[:decalage] #ascii de 97 a 122

        self.assertEqual(transcription('a',decalage,new_liste_minuscule,'cryptage'),'d')
        self.assertEqual(transcription('b',decalage,new_liste_minuscule,'cryptage'),'e')
        self.assertEqual(transcription('e',decalage,new_liste_minuscule,'decryptage'),'b')
        self.assertNotEqual(transcription('A',decalage,new_liste_majuscule,'cryptage'),'B')

        decalage = 5

        new_liste_minuscule = list_minuscule[decalage:]+list_minuscule[:decalage] #ascii de 65 a 90
        new_liste_majuscule = list_majuscule[decalage:]+list_majuscule[:decalage] #ascii de 97 a 122
        self.assertEqual(transcription('a',decalage,new_liste_minuscule,'cryptage'),'f')
        self.assertEqual(transcription('b',decalage,new_liste_minuscule,'cryptage'),'g')
        self.assertEqual(transcription('g',decalage,new_liste_minuscule,'decryptage'),'b')

